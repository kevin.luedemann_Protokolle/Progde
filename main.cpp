#include <iostream>
#include <math.h>
#include <fstream>

using namespace std;

int main(){

double v1 =0,v2 =0, qerg[10], sigma =0,sigmaein[10], mittel = 0, temp =0, fmittel = 0, gmittel = 0;
for(int i =0; i<10;i++){qerg[i]=0; sigmaein[i]=0;}

ifstream f;
ofstream g;
const double gerd =9.81, eta = 0.000018, drho = 873, d = 0.006, U = 617;
f.open("messergebnisse.dat");
g.open("ergebnisse.dat");
for(int i =0; i<5;i++){
f >> temp;
f >> v1;
f >> v2;

v1 =1/(1000*v1);
v2 =1/(1000*v2);


qerg[i] = (6*3.14159265358979*eta*d*sqrt((9*eta*v1)/(2*drho*gerd))*(v1+v2))/(U);
if(temp==1 | temp ==3){
    mittel = mittel + (qerg[i]/2);
}
else {mittel = mittel + qerg[i];}
cout << qerg[i] << endl;

sigmaein[i] = sqrt(
            (0.000001*0.000001)*((((3/2)*sqrt(eta)*(6*3.14159265358979*d*sqrt((9*v1)/(2*drho*gerd))*(v1+v2)))/(U))*(((3/2)*sqrt(eta)*(6*3.14159265358979*d*sqrt((9*v1)/(2*drho*gerd))*(v1+v2)))/(U)))
            + (0.001*0.001)*(((6*3.14159265358979*eta*sqrt((9*eta*v1)/(2*drho*gerd))*(v1+v2))/(U))*((6*3.14159265358979*eta*sqrt((9*eta*v1)/(2*drho*gerd))*(v1+v2))/(U)))
            + (25) * (((-1/sqrt(drho*drho*drho))*(6*3.14159265358979*eta*d*sqrt((9*eta*v1)/(2*gerd))*(v1+v2))/(U))*((-1/sqrt(drho*drho*drho))*(6*3.14159265358979*eta*d*sqrt((9*eta*v1)/(2*gerd))*(v1+v2))/(U)))
            + (((-1/(U*U))*(6*3.14159265358979*eta*d*sqrt((9*eta*v1)/(2*drho*gerd))*(v1+v2)))*((-1/(U*U))*(6*3.14159265358979*eta*d*sqrt((9*eta*v1)/(2*drho*gerd))*(v1+v2))))
            + ((1/200) * (1/200)) * (((6*3.14159265358979*eta*d*sqrt((9*eta*v1)/(2*drho*gerd)))/(U))*((6*3.14159265358979*eta*d*sqrt((9*eta*v1)/(2*drho*gerd)))/(U)))
            + ((1/200) * (1/200)) * ((((3/2)*sqrt(v1)*(6*3.14159265358979*eta*d*sqrt((9*eta)/(2*drho*gerd)))/(U))+((1/2)*(1/sqrt(v1)))*((6*3.14159265358979*eta*d*sqrt((9*eta)/(2*drho*gerd))*v2)/(U)))
                           *((3/2)*sqrt(v1)*(6*3.14159265358979*eta*d*sqrt((9*eta)/(2*drho*gerd)))/(U))+((1/2)*(1/sqrt(v1)))*((6*3.14159265358979*eta*d*sqrt((9*eta)/(2*drho*gerd))*v2)/(U)))
            );}

mittel /= 5;
cout << "mittelwert= " << mittel << endl;
g << "mittelwert= " << mittel << endl;

for(int i =0; i<5 ;i++){
    if(i ==0 | i==2){
      sigma = sigma + (((qerg[i]/2) - mittel)*((qerg[i]/2) - mittel));
    }
else {sigma = sigma + ((qerg[i] - mittel)*(qerg[i] - mittel));}
}

sigma /= 4;
cout << "sigma ^2= " << sigma << endl;
g << "sigma ^2= " << sigma << endl;
sigma = sqrt(sigma);

cout << "sigma= " << sigma << endl;
g << "sigma= " << sigma << endl;
fmittel = 1.15*(sigma / sqrt(5));
cout << "Fehler des Mittelwertes= " << fmittel << endl<< endl;
g << "Fehler des Mittelwertes= " << fmittel << endl<< endl;
cout << "(" << mittel <<"+-" << sigma << ")" << "c" <<endl;
g << "(" << mittel <<"+-" << sigma << ")" << "c" <<endl;
cout << "Fehler des Einzelwertes" << endl;
g << "Fehler des Einzelwertes" << endl;
for (int i =0; i<5;i++){
    cout << qerg[i] << " dessen Fehler:  " << sigmaein[i] << endl;
    g << i << " " <<  qerg[i] << " " << sigmaein[i] << endl;
}
f.close();
g.close();

return 0;
}
