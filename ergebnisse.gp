reset
set terminal postscript enhanced color 
set output "Millikanversuch.eps"
set xrange [-1:5]
set yrange [0e-19:5e-19]
set grid

set title "Millikanversuch"
set xlabel "Messungen []"
set ylabel "Ladung [c]"
set ytics 0.5e-19
plot "ergebnisse.dat" u 1:2:3 w e t "Messwerte"

set output
!epstopdf --autorotate=All  "Millikanversuch.eps"

#pause -1
